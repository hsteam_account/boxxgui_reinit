
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QTabWidget,QListWidget, QListView, QDial, QWidget
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint

import os, sys
import datetime
import windows
from programData import CMaterial, SMaterial
def shoeSelectionChanged(button, owner):
    owner.shoeValueChanged(button.index)
def clothSelectionChanged(button, owner):
    owner.clothValueChanged(button.index)
class Button():
    def __init__(self, normal, selected, time):
        self.normal=normal
        self.selected=selected
        self.time=time
    def select(self):
        self.func(self,self.owner)

    def selectButton(self, all):
        for item in all:
            item.selected.hide()
            item.normal.show()
        self.selected.show()
        self.normal.hide()
        self.select()

class MaterialWindow(QObject):

        def __init__(self, parent=None):
                super(MaterialWindow, self).__init__(parent)
                Form, Window = uic.loadUiType("materialChoice.ui")
                self.window = Window()
                self.form = Form()
                self.form.setupUi(self.window)
                
                # Shoes
                self.shoeMaterials=[SMaterial("None", 0,0), SMaterial("Leather", 1,1), SMaterial("Sports", 1,1)]
                self.shoeItems=self.getItems(QWidget, 'shoe', 3)
                self.setUpItems(self.shoeItems, self.shoeMaterials)
                self.shoeSelectionButtons=self.getSelectionButtons(['sNone', 'sLeather','sSports'], self.shoeMaterials)
                self.initButtons(self.shoeSelectionButtons,shoeSelectionChanged, 0)
                #cloth
                self.clothMaterials=[CMaterial("None", 0,0,0), CMaterial("Silk", 1,1,1), CMaterial("Leather", 1,1,0),CMaterial("Sports", 1,1,1)]
                self.clothItems=self.getItems(QWidget, 'cloth', 4)
                self.setUpItems(self.clothItems, self.clothMaterials)
                self.clothSelectionButtons=self.getSelectionButtons(['cNone','cSilk', 'cLeather','cSports'], self.clothMaterials)
                self.initButtons(self.clothSelectionButtons, clothSelectionChanged, 0)

                self.nextButton=self.getItem(QPushButton, 'nextButton')
                self.nextButton.clicked.connect(self.next)
                self.shoeMaterial=self.shoeMaterials[0]
                self.clothMaterial=self.clothMaterials[0]



        def getSelectionButtons(self, ids, values):
            selectionButtons=[]
            for i in range(0,len(ids)):
                selectionButtons.append(Button(self.getButton(ids[i]+'Normal'), self.getButton(ids[i]+'Selected'),values[i] ))
            return selectionButtons
        def show(self):

                self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height());
                #self.window.show()
                self.window.showFullScreen()

                #then a animation:
                animation = QPropertyAnimation(self.window, b"pos");
                animation.setDuration(10000);
                animation.setStartValue(self.window.pos());
                animation.setEndValue(QPoint(0,0));

                #to slide in call
                animation.start();


        def initButtons(self, buttons, func, initialIndex):
                for button in buttons:
                    print(buttons.index(button))
                    button.selected.hide()
                    button.index=buttons.index(button)
                    button.owner=self
                    button.func=func
                    button.normal.clicked.connect(self.selectButtons(button, buttons))
                buttons[initialIndex].selected.show()
                buttons[initialIndex].normal.hide()
        def selectButtons(self, button, buttons):
                return lambda  x: button.selectButton( buttons)
        def getButton(self, name):
            return self.window.findChild(QPushButton,name)

        def setUpItems(self, items, materials):
            for i in range(0, len(items)):
                materials[i].item=items[i]
                if(i>0):
                    items[i].hide()
        def getItems(self, type,id, num ):
            items=[]
            for i in range (1, num+1):
                items.append(self.window.findChild(type,id+str(i)))
            return items
        def getItem(self, type,id):
            return self.window.findChild(type, id)
        def shoeValueChanged(self,value):
            shoeMaterial=self.valueChanged(value, self.shoeMaterials)
            if shoeMaterial==None:
                self.shoeMaterial=self.shoeMaterials[0]
            else:
                self.shoeMaterial=shoeMaterial
        def clothValueChanged(self,value):
            clothMaterial=self.valueChanged(value, self.clothMaterials)
            if clothMaterial==None:
                self.clothMaterial=self.clothMaterials[0]
            else:
                self.clothMaterial=clothMaterial

        def valueChanged(self,value, materials):
            index=value
            material=None
            for i in range(len(materials)):
                if materials[i].item!=None:
                    if i==index:
                        materials[i].item.show()
                        material=materials[i]
                    else:
                        materials[i].item.hide()
            return material
        def show(self):
                self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height());
                #self.window.show()
                self.window.showFullScreen()
        def next(self):
            if self.shoeMaterial.name=="None" and self.clothMaterial.name=="None":
                return
            self.nextWindow=windows.ChooseWindow()
            self.nextWindow.init(self.shoeMaterial, self.clothMaterial)
            self.nextWindow.show()
            self.window.close()
