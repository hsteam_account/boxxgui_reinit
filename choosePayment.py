from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QTabWidget
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint

import os, sys
from programData import CMaterial, SMaterial, Data
import datetime
import windows

class ChoosePayment(QObject):

        def __init__(self, parent=None):
                super(ChoosePayment, self).__init__(parent)

                ui_file = "choosePayment.ui"
                # self.line = self.window.findChild(QLineEdit, 'lineEdit')
                Form, Window = uic.loadUiType(ui_file)
                self.window = Window()
                self.form = Form()
                self.form.setupUi(self.window)

                self.backButton=self.getItem(QPushButton, 'backButton')
                self.backButton.clicked.connect(self.back)

                self.nfcButton=self.getItem(QPushButton, 'nfcButton')
                self.nfcButton.clicked.connect(self.nfcPressed)
        def show(self):

                self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height());
                #self.window.show()
                self.window.showFullScreen()
        def back(self):
            self.previousWindow=windows.ChooseWindow()
            self.previousWindow.show()
            self.previousWindow.init(self.sMaterial, self.cMaterial)
            self.window.close()
        def nfcPressed(self):
                self.nfcWin=windows.NfcWindow()
                self.nfcWin.data=self.data
                self.nfcWin.sMaterial=self.sMaterial
                self.nfcWin.cMaterial=self.cMaterial
                self.nfcWin.show()
                self.window.close()
                self.nfcWin.check()

        def next(self):
                self.processWin = windows.ProcessWindow()
                #self.timer.stop()
                self.processWin.show()
                self.data.calculateProgram()
                self.processWin.start_Timer(self.data)
                self.window.close()
        def getItem(self, type,id):
            return self.window.findChild(type, id)