from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint

import os, sys
import datetime

import windows


class StartWindow(QObject):
 
	def tick(self):
		print('start tick')

		clockText = self.window.findChild(QLabel, 'clockLabel')

		now = datetime.datetime.now()
		#timestr = now.strftime("%H:%M")
		#clockText.setText(timestr)

	def __init__(self, parent=None):
		super(StartWindow, self).__init__(parent)

		ui_file = "start.ui"
	
		# self.line = self.window.findChild(QLineEdit, 'lineEdit')


		Form, Window = uic.loadUiType("start.ui")

		self.window = Window()

		self.form = Form()
		self.form.setupUi(self.window)

		self.timer = QTimer()
		self.timer.timeout.connect(self.tick)
		self.timer.start(1000)


		btn = self.window.findChild(QPushButton, 'startButton')
		btn.clicked.connect(self.start_handler)

		debugbtn = self.window.findChild(QPushButton, 'debugButton')
		debugbtn.clicked.connect(self.debugHandler)

		leftImgLabel = self.window.findChild(QLabel, 'leftPartLabel')
		rightImgLabel = self.window.findChild(QLabel, 'rightPartLabel')

		pixmap = QPixmap('shirt1.png')
		leftImgLabel.setPixmap(pixmap)


		pixmap = QPixmap('shirt2.png')
		rightImgLabel.setPixmap(pixmap)

	def __del__(self):
		try:
			self.timer.stop()
		except:
			pass

	def show(self):

		self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height());
		#self.window.show()
		self.window.showFullScreen()

		#then a animation:
		animation = QPropertyAnimation(self.window, b"pos");
		animation.setDuration(10000);
		animation.setStartValue(self.window.pos());
		animation.setEndValue(QPoint(0,0));
		 
		#to slide in call
		animation.start();


	

	def start_handler(self):
		print("pressed process")

		self.processWin = windows.ProcessWindow()
		self.timer.stop()
		self.processWin.show()
		self.processWin.start_Timer(1)
		self.window.close()

	def debugHandler(self):
		print("pressed process")

		self.debugWin = windows.DebugWindow()
		self.timer.stop()

		self.window.close()