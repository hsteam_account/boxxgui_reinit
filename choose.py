from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton,QLCDNumber, QFrame
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint, QThread
from programData import Data
import os, sys
import datetime
import time, threading
import windows
def setUVTime(button, owner):
    owner.data.uvTime=button.time
    owner.setTimes()
def setDryTime(button, owner):
    owner.data.dryTime=button.time
    owner.setTimes()

class Button():
    def __init__(self, normal, selected, time):
        self.normal=normal
        self.selected=selected
        self.time=time
    def select(self):
        self.func(self,self.owner)

    def selectButton(self, all):
        for item in all:
            item.selected.hide()
            item.normal.show()
        self.selected.show()
        self.normal.hide()
        self.select()

class ChooseWindow(QObject):
    def __init__(self, parent=None):
        super(ChooseWindow, self).__init__(parent)
    # self.line = self.window.findChild(QLineEdit, 'lineEdit')
        Form, Window = uic.loadUiType("choose.ui")
        self.window = Window()
        self.form = Form()
        self.form.setupUi(self.window)

        self.data=Data()

        

        self.uvTime=self.window.findChild(QLCDNumber, 'uvTime')
        self.dryTime=self.window.findChild(QLCDNumber, 'dryTime')
        self.ironTime=self.window.findChild(QLCDNumber, 'ironTime')
        self.sumTime=self.window.findChild(QLCDNumber, 'sumTime')

        self.uvTimeFrame=self.window.findChild(QFrame,'uvTimeFrame')
        self.dryTimeFrame=self.window.findChild(QFrame,'dryTimeFrame')
        self.ironTimeFrame=self.window.findChild(QFrame,'ironTimeFrame')


        self.uvTimeFrame.hide()
        self.dryTimeFrame.hide()
        self.ironTimeFrame.hide()

        self.uvMinus=self.window.findChild(QPushButton,'uvMinus')
        self.uvPlus=self.window.findChild(QPushButton,'uvPlus')

        self.dryMinus=self.window.findChild(QPushButton,'dryMinus')
        self.dryPlus=self.window.findChild(QPushButton,'dryPlus')

        self.ironMinus=self.window.findChild(QPushButton,'ironMinus')
        self.ironPlus=self.window.findChild(QPushButton,'ironPlus')

        self.uvMinus.clicked.connect(self.decrementUVTime)
        self.uvPlus.clicked.connect(self.incrementUVTime)
        self.dryMinus.clicked.connect(self.decrementDryTime)
        self.dryPlus.clicked.connect(self.incrementDryTime)
        self.ironMinus.clicked.connect(self.decrementIronTime)
        self.ironPlus.clicked.connect(self.incrementIronTime)

        littleStinkNormal=self.window.findChild(QPushButton,'littleStinkNormal')
        littleStinkSelected=self.window.findChild(QPushButton,'littleStinkSelected')
        mediumStinkNormal=self.window.findChild(QPushButton,'mediumStinkNormal')
        mediumStinkSelected=self.window.findChild(QPushButton,'mediumStinkSelected')
        muchStinkNormal=self.window.findChild(QPushButton,'muchStinkNormal')
        muchStinkSelected=self.window.findChild(QPushButton,'muchStinkSelected')

        littleDryNormal=self.window.findChild(QPushButton,'littleDryNormal')
        littleDrySelected=self.window.findChild(QPushButton,'littleDrySelected')
        mediumDryNormal=self.window.findChild(QPushButton,'mediumDryNormal')
        mediumDrySelected=self.window.findChild(QPushButton,'mediumDrySelected')
        muchDryNormal=self.window.findChild(QPushButton,'muchDryNormal')
        muchDrySelected=self.window.findChild(QPushButton,'muchDrySelected')



        self.uvButtons=[Button(littleStinkNormal,littleStinkSelected, 5), Button(mediumStinkNormal, mediumStinkSelected,10 ), Button(muchStinkNormal, muchStinkSelected, 30)]
        self.dryButtons=[Button(littleDryNormal,littleDrySelected, 5), Button(mediumDryNormal, mediumDrySelected,10 ), Button(muchDryNormal, muchDrySelected, 20)]

        self.initButtons(self.uvButtons,setUVTime)
        self.initButtons(self.dryButtons,setDryTime)

        self.advancedModeNormal=self.window.findChild(QPushButton,'advancedModeNormal')
        self.advancedModeSelected=self.window.findChild(QPushButton,'advancedModeSelected')
        self.advancedModeSelected.hide()

        self.advancedModeNormal.clicked.connect(self.activateAdvancedMode)
        self.advancedModeSelected.clicked.connect(self.deActivateAdvancedMode)

        self.ironNormal=self.window.findChild(QPushButton,'ironNormal')
        self.ironSelected=self.window.findChild(QPushButton,'ironSelected')
        self.ironSelected.hide()

        self.ironNormal.clicked.connect(self.activateIroning)
        self.ironSelected.clicked.connect(self.deactivateIroning)
        

        self.backButton=self.window.findChild(QPushButton,'backButton')
        self.startButton=self.window.findChild(QPushButton,'startButton')
        self.backButton.clicked.connect(self.back)
        self.startButton.clicked.connect(self.start)
        self.setTimes()

    def incrementUVTime(self):
        self.data.uvTime+=1
        self.setTimes()
    def decrementUVTime(self):
        self.data.uvTime-=1
        if self.data.uvTime<0:
            self.data.uvTime=0
        self.setTimes()

    def decrementDryTime(self):
        self.data.dryTime-=1
        if self.data.dryTime<0:
            self.data.dryTime=0
        self.setTimes()
    def incrementDryTime(self):
        self.data.dryTime+=1
        self.setTimes()
    

    def decrementIronTime(self):
        self.data.ironTime-=1
        if self.data.ironTime<0:
            self.data.ironTime=0
        self.setTimes()
    def incrementIronTime(self):
        self.data.ironTime+=1
        self.setTimes()


    def activateIroning(self):
        self.data.ironTime=20
        self.ironSelected.show()
        self.ironNormal.hide()
        self.setTimes()
    def deactivateIroning(self):
        self.data.ironTime=0
        self.ironNormal.show()
        self.ironSelected.hide()
        self.setTimes()
    def activateAdvancedMode(self):
        self.advancedModeNormal.hide()
        self.advancedModeSelected.show()
        self.uvTimeFrame.show()
        self.dryTimeFrame.show()
        self.ironTimeFrame.show()
    def deActivateAdvancedMode(self):
        self.advancedModeSelected.hide()
        self.advancedModeNormal.show()
        self.uvTimeFrame.hide()
        self.dryTimeFrame.hide()
        self.ironTimeFrame.hide()
    def start(self):
        self.processWin = windows.ProcessWindow()
        #self.timer.stop()
        self.processWin.show()
        self.data.calculateProgram()
        self.processWin.start_Timer(self.data)
        self.window.close()
    def back(self):
        self.startWin = windows.Home()
        self.window.close() 
    def initButtons(self, buttons, func):
        for button in buttons:
            print(button.time)
            button.selected.hide()
            button.owner=self
            button.func=func
            button.normal.clicked.connect(self.selectButtons(button, buttons))
        buttons[1].selected.show()
        buttons[1].normal.hide()
    def selectButtons(self, button, buttons):
        return lambda  x: button.selectButton( buttons)
    def setTimes(self):
        self.uvTime.display(self.data.uvTime)
        self.dryTime.display(self.data.dryTime)
        self.ironTime.display(self.data.ironTime)
        self.data.fullTime=self.data.uvTime+self.data.dryTime+self.data.ironTime+self.data.ventTime+(float(2)/3)
        if self.data.ironTime>0:
            self.data.fullTime+=1
        self.sumTime.display(self.data.fullTime)
        print(self.data.fullTime)

    def show(self):

        self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height())
        #self.window.show()
        self.window.showFullScreen()

        #then a animation:
        animation = QPropertyAnimation(self.window, b"pos")
        animation.setDuration(10000)
        animation.setStartValue(self.window.pos())
        animation.setEndValue(QPoint(0,0))
         
        #to slide in call
        animation.start()
