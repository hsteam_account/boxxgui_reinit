try:
    import smbus
    noAdapterHardware=False
except:
    noAdapterHardware=True
import time

address = 0x18

VAPOR_CMD = 0x0C
PUMP_CMD = 0x0D
UV1_CMD = 0x0E
UV2_CMD = 0x0F

FAN1_CMD = 0x08
FAN2_CMD = 0x09
FAN3_CMD = 0x0A
FAN4_CMD = 0x0B

READADC_CMD_L = 0x06
READADC_CMD_H = 0x07
class Adapter():
    def __init__(self, parent=None):
        self.fansOn=True #Will be shut down
        self.uv1On=True #Will be shut down
        self.uv2On=True #Will be shut down
        self.pumpOn=True #Will be shut down
        self.vaporOn=True #Will be shut down
        if noAdapterHardware:
            pass
        else:
            self.bus = smbus.SMBus(1)

    def turnOn(self, cmd):
        if noAdapterHardware:
            print("turnOn", cmd)
        else:
            self.bus.write_byte_data(address, cmd, 1)
            data = self.bus.read_byte(address)

    def turnOff(self, cmd):
        if noAdapterHardware:
            print("turnOn", cmd)
        else:
            self.bus.write_byte_data(address, cmd, 0)
            data = self.bus.read_byte(address)

    def turnOnPump(self):
        if self.pumpOn==False:
            self.pumpOn=True
            self.turnOn(PUMP_CMD)
    def turnOffPump(self):
        if self.pumpOn:
            self.pumpOn=False
            self.turnOff(PUMP_CMD)
    def turnOnVapor(self):
        if self.vaporOn==False:
            self.vaporOn=True
            self.turnOn(VAPOR_CMD)
    def turnOffVapor(self):
        if self.vaporOn:
            self.vaporOn=False
            self.turnOff(VAPOR_CMD)
    def turnOnUV1(self):
        if self.uv1On==False:
            self.uv1On=True
            self.turnOn(UV1_CMD)
    def turnOffUV1(self):
        if self.uv1On:
            self.uv1On=False
            self.turnOff(UV1_CMD)
    def turnOnUV2(self):
        if self.uv2On==False:
            self.uv2On=True
            self.turnOn(UV2_CMD)
    def turnOffUV2(self):
        if self.uv2On:
            self.uv2On=False
            self.turnOff(UV2_CMD)
    def turnOnFans(self):
        if self.fansOn==False:
            self.fansOn=True
            self.turnOn(FAN1_CMD)
            self.turnOn(FAN2_CMD)
            self.turnOn(FAN3_CMD)
            self.turnOn(FAN4_CMD)
    def turnOffFans(self):
        if self.fansOn:
            self.fansOn=False
            self.turnOff(FAN1_CMD)
            self.turnOff(FAN2_CMD)
            self.turnOff(FAN3_CMD)
            self.turnOff(FAN4_CMD)

    def triggerSwitch(self,cmd):
        self.bus.write_byte_data(address, cmd, 1)
        data = self.bus.read_byte(address)
        print(data)
        time.sleep(6)
        self.bus.write_byte_data(address, cmd, 0)
        data = self.bus.read_byte(address)
        print(data)

    def getTemperature(self):
        if noAdapterHardware:
            print("turnOn", cmd)
            return
        self.bus.write_byte_data(address, READADC_CMD_L, 1)
        data1 = self.bus.read_byte(address)
        self.bus.write_byte_data(address, READADC_CMD_H, 1)
        data2 = self.bus.read_byte(address)
        val = (data1 + (data2<<8))/4095.0*3.3*33.33333
        print("Temp: ", val)

    def getCurrent(self):
        if noAdapterHardware:
            print("turnOn", cmd)
            return
        self.bus.write_byte_data(address, READADC_CMD_L, 0)
        data1 = self.bus.read_byte(address)
        self.bus.write_byte_data(address, READADC_CMD_H, 0)
        data2 = self.bus.read_byte(address)
        offset = 0.044
        zeropos = 1.65
        apervolt = 15.15151515
        val = ((data1 + (data2<<8))/4095.0*3.3+offset-zeropos)*apervolt
        print("Current: ", val)

    