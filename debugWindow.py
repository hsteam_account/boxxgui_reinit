#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial 

In this example, we create a skeleton
of a calculator using QGridLayout.

Author: Jan Bodnar
Website: zetcode.com 
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import (QWidget, QGridLayout, QHBoxLayout,QVBoxLayout, QPushButton, QApplication, QSizePolicy,QLabel)
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint
from PyQt5.QtGui import QFont
import windows
from hardware import Hardware

fontSize="font: 20pt Helvetica MS; \n"
red="background-color: red"
green="background-color: green"
blue="background-color: blue"
redStyle=fontSize+red
greenStyle=fontSize+green
blueStyle=fontSize+blue
class ToggleLabel():
    def __init__(self, grid, text, onClickOn, onClickOff, time, owner):
        h=QHBoxLayout()
        l=QLabel(text)
        l.setStyleSheet(fontSize)
        b1=QPushButton("Start")
        b2=QPushButton("Mode: Open")
        b1.setStyleSheet(greenStyle)
        b1.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        b1.setCheckable(False)
        b2.setStyleSheet(blueStyle)
        b2.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        b2.setCheckable(False)
        h.addWidget(l)
        h.addWidget(b1)
        h.addWidget(b2)
        grid.addLayout(h)
        self.label=l
        self.button1=b1
        self.button2=b2
        self.horizontal=h
        self.owner=owner
        self.onClickedOnFunc=onClickOn
        self.onClickedOffFunc=onClickOff
        self.button1.clicked.connect(self.onClicked)
        self.button2.clicked.connect(self.onModeChanged)
        self.time=time
        self.on=False
        self.activated=False
    def onModeChanged(self):
        if self.activated:
            return
        self.on=False if self.on else True
        if self.on:
            self.button2.setText("Mode: Close")
        else:
            self.button2.setText("Mode: Open")

    def onClicked(self):
        if self.activated:
            return
        else:
            self.activated=True
            self.button1.setStyleSheet(redStyle)
            self.timer = QTimer()
            if self.on:
                self.onClickedOffFunc(self,self.owner)
                self.mode="Closing"
                self.button1.setText(self.mode)
            else:
                self.onClickedOnFunc(self,self.owner)
                self.mode="Opening"
                self.button1.setText(self.mode)
            self.timer.timeout.connect(self.tick)
            self.countdownTime=self.time
            self.timer.start(1000)
    def tick(self):
        self.countdownTime-=1
        self.button1.setText(self.mode+"("+str(self.countdownTime)+")")
        if(self.countdownTime==0):
            self.activated=False
            self.button1.setText("Start")
            self.button1.setStyleSheet(greenStyle)
            self.timer.stop()
        
class Label():
    def __init__(self, grid, text, onClick ,owner):
        h=QHBoxLayout()
        l=QLabel(text)
        l.setStyleSheet(fontSize)
        b=QPushButton("OFF")
        b.setStyleSheet(redStyle)
        b.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        b.setCheckable(True)
        h.addWidget(l)
        h.addWidget(b)
        grid.addLayout(h)
        self.label=l
        self.button=b
        self.horizontal=h
        self.owner=owner
        self.onClickedFunc=onClick
        self.button.clicked[bool].connect(self.onClicked)

    def onClicked(self,on):
        if on:
            self.button.setStyleSheet(greenStyle)
            self.button.setText("ON")
        else:
            self.button.setStyleSheet(redStyle)
            self.button.setText("OFF")

        self.onClickedFunc(self,self.owner, on)
        
class DebugWindow(QWidget):
    
    def __init__(self):
        super().__init__()
        self.hardware=Hardware()
        self.labelInfos=[
            ("Dampf", self.hardware.activateSteam, self.hardware.deactivateSteam),
            ("Vakuumpumpe", self.hardware.activatePump, self.hardware.deactivatePump),
            ("Heizung", self.hardware.activateHeat, self.hardware.deactivateHeat),
            ("Äußere Lüfter", self.hardware.activateOuterFans, self.hardware.deactivateOuterFans),
            ("Duftlüfter", self.hardware.activateOdorFan, self.hardware.deactivateOdorFan),
            ("Kleidungslüfter", self.hardware.activateClothingFan, self.hardware.deactivateClothingFan),
            ("Schuhlüfter", self.hardware.activateShoeFan, self.hardware.deactivateShoeFan),
            ("Mutterlüfter", self.hardware.activateMotherFan, self.hardware.deactivateMotherFan),
            ("Adapterlüfter", self.hardware.turnOnAllFans, self.hardware.turnOffAllFans),
            ("UVs", self.hardware.turnOnAllUVs, self.hardware.turnOffAllUVs),
        ]
        self.toggleInfos=[
            ("Tür", self.hardware.openDoor, self.doNothing,1 ),
            ("Obere Klappe", self.hardware.openHatchMisc, self.hardware.closeHatchMisc, 21),
            ("Untere Klappe", self.hardware.openHatchExit, self.hardware.closeHatchExit, 21),
            ("Duftklappen", self.hardware.openHatchBottle, self.hardware.closeHatchBottle, 21),
        ]
        self.labels=[]
        self.toggles=[]
        self.initUI()
    def doNothing(self):
        pass
    def tick(self):
        #print('main tick')
        pass  
    def initUI(self):
        
        grid = QVBoxLayout()
        self.setLayout(grid)
        self.setWindowTitle('Debug')
        self.timer = QTimer()
        self.timer.timeout.connect(self.tick)
        self.timer.start(1000)
        for label in self.labelInfos:
            self.labels.append(Label(grid, label[0],self.dLambda(label[1], label[2]), self))
        for toggle in self.toggleInfos:
            self.toggles.append(ToggleLabel(grid,toggle[0], self.toggleFunc(toggle[1]), self.toggleFunc(toggle[2]), toggle[3], self))
        self.backButton=QPushButton("zurück")
        self.backButton.setStyleSheet(fontSize)
        self.backButton.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.backButton.clicked.connect(self.backHandler)
        grid.addWidget(self.backButton)
        self.showFullScreen()
    def backHandler(self):
        self.hardware.clear()
        self.startWin = windows.Home()
        self.close()
    def printOn(self):
        print("ON")
    def printOff(self):
        print("OFF")

    def toggleFunc(self, func):
        return lambda toggle, self: func()
    def dLambda(self, onFunc, offFunc):
        return lambda label, self, on: self.funcs(on, onFunc, offFunc)
    def funcs(self, on, onFunc, offFunc):
        if on: 
            onFunc()
        else:
            offFunc()

    
