
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QTabWidget,QListWidget, QListView, QDial, QWidget
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint,QThread

import os, sys
import datetime
import windows
from programData import CMaterial, SMaterial, Data, NfcData
try:
    from pynfc import Nfc, Desfire, Timeout
    noNfc=False
except:
    noNfc=True

def checkForConnection(owner):
    if noNfc:
        return
    else:
        for target in n.poll():
            try:
                print(target.uid, target.auth(DESFIRE_DEFAULT_KEY if type(target) == Desfire else MIFARE_BLANK_TOKEN))
                owner.nfc.id=target.uid
                return
            except TimeoutException:
                pass
if noNfc:
    pass
else:
    n = Nfc("pn532_i2c:/dev/i2c-1")
    DESFIRE_DEFAULT_KEY = b'\x00' * 8
    MIFARE_BLANK_TOKEN = b'\xFF' * 1024 * 4
class WorkerThread(QThread):
    def __init__(self, owner, parent=None):
        super(WorkerThread, self).__init__(parent)
        self.owner=owner
    def run(self):
        checkForConnection(self.owner)
class NfcWindow(QObject):

        def __init__(self, parent=None):
                super(NfcWindow, self).__init__(parent)
                Form, Window = uic.loadUiType("nfcWindow.ui")
                self.window = Window()
                self.form = Form()
                self.form.setupUi(self.window)
                self.nfc=NfcData()
                self.notOkay=True
        def next(self):
            self.nextWindow=windows.acceptWindow()
            self.nextWindow.data=self.data
            self.nextWindow.sMaterial=self.sMaterial
            self.nextWindow.cMaterial=self.cMaterial

        def check(self):
            print("checking")
            self.x=WorkerThread(self)
            self.x.finished.connect(self.okay_handler)
            self.x.start()


        def show(self):

                self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height());
                #self.window.show()
                self.window.showFullScreen()

                #then a animation:
                animation = QPropertyAnimation(self.window, b"pos");
                animation.setDuration(10000);
                animation.setStartValue(self.window.pos());
                animation.setEndValue(QPoint(0,0));

                #to slide in call
                animation.start();


        def okay_handler(self):
            if(self.notOkay):
                self.x.exit()
                self.notOkay=False
                #checkForConnection()
                self.nextWindow = windows.AcceptWindow()
                self.nextWindow.data=self.data
                self.nextWindow.sMaterial=self.sMaterial
                self.nextWindow.cMaterial=self.cMaterial
                self.nextWindow.nfc=self.nfc
                self.nextWindow.show()
                self.nextWindow.init()
                self.window.close() 
        