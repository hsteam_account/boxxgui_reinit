from hardware import Hardware


def uVState(self,processText):
    processText.setText("Irradiation...")
    self.hardware.closeHatchExit()
    self.hardware.closeHatchMisc()
    self.hardware.closeHatchBottle()

    self.hardware.turnOnAllUVs()
    self.hardware.turnOnAdapterPumps()

    self.hardware.turnOnAllFans()
    self.hardware.deactivateOuterFans()
    self.hardware.activateShoeFan()
    self.hardware.activateClothingFan()
    self.hardware.activateMotherFan()
    self.hardware.deactivateOdorFan()

    self.hardware.deactivateHeat()
    self.hardware.deactivateSteam()
    self.hardware.SetLEDsBoxx(0,0,1)
def turnOffClothUV(self, processText):
    self.hardware.turnOffAllUVs()
def turnOffShoeUV(self, processTest):
    self.hardware.turnOffAdapterPumps()
def turnOnHeat(self,processText):
    self.hardware.activateHeat()

def uVHeatState(self,processText, shoe, cloth):
    processText.setText("Irradiation...")
    self.hardware.closeHatchExit()
    self.hardware.closeHatchMisc()
    self.hardware.closeHatchBottle()

    self.hardware.turnOnAllUVs()
    self.hardware.turnOnAdapterPumps()

    self.hardware.turnOnAllFans()
    self.hardware.deactivateOuterFans()
    self.hardware.activateShoeFan()
    self.hardware.activateClothingFan()
    self.hardware.activateMotherFan()
    self.hardware.deactivateOdorFan()

    self.hardware.activateHeat()
    self.hardware.deactivateSteam()
    self.hardware.SetLEDsBoxx(0,0,1)

def steamState(self,processText):
    processText.setText("Irradiation...")
    self.hardware.closeHatchExit()
    self.hardware.closeHatchMisc()
    self.hardware.closeHatchBottle()

    self.hardware.turnOffAllUVs()
    self.hardware.turnOffAdapterPumps()


    self.hardware.turnOnAllFans()
    self.hardware.deactivateOuterFans()
    self.hardware.activateShoeFan()
    self.hardware.activateClothingFan()
    self.hardware.activateMotherFan()
    self.hardware.deactivateOdorFan()
    
    self.hardware.activateHeat()
    self.hardware.activateSteam()

    self.hardware.SetLEDsBoxx(0,0,1)

def airOutState(self,processText):
    processText.setText("Irradiation...")
    self.hardware.openHatchExit()
    self.hardware.openHatchMisc()
    self.hardware.closeHatchBottle()

    self.hardware.turnOffAllUVs()
    self.hardware.turnOffAdapterPumps()

    self.hardware.turnOnAllFans()
    self.hardware.activateOuterFans()
    self.hardware.activateShoeFan()
    self.hardware.activateClothingFan()
    self.hardware.activateMotherFan()
    self.hardware.deactivateOdorFan()
    
    self.hardware.activateHeat()
    self.hardware.deactivateSteam()

    self.hardware.SetLEDsBoxx(0,0,1)

def circulationState(self,processText):
    processText.setText("Irradiation...")
    self.hardware.closeHatchExit()
    self.hardware.closeHatchMisc()
    self.hardware.closeHatchBottle()

    self.hardware.turnOffAllUVs()
    self.hardware.turnOffAdapterPumps()

    self.hardware.turnOnAllFans()
    self.hardware.deactivateOuterFans()
    self.hardware.activateShoeFan()
    self.hardware.activateClothingFan()
    self.hardware.activateMotherFan()
    self.hardware.deactivateOdorFan()
    
    self.hardware.activateHeat()
    self.hardware.deactivateSteam()

    self.hardware.SetLEDsBoxx(0,0,1)

def scentState(self,processText):
    processText.setText("Irradiation...")
    self.hardware.closeHatchExit()
    self.hardware.closeHatchMisc()
    self.hardware.openHatchBottle()

    self.hardware.turnOffAllUVs()
    self.hardware.turnOffAdapterPumps()

    self.hardware.turnOnAllFans()
    self.hardware.deactivateOuterFans()
    self.hardware.activateShoeFan()
    self.hardware.activateClothingFan()
    self.hardware.activateMotherFan()
    self.hardware.activateOdorFan()
    
    self.hardware.activateHeat()
    self.hardware.deactivateSteam()

    self.hardware.SetLEDsBoxx(0,0,1)