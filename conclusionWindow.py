
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QTabWidget,QListWidget, QListView, QDial, QWidget
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint,QThread

import os, sys
import datetime
import windows
from programData import CMaterial, SMaterial, Data, NfcData



class AcceptWindow(QObject):

        def __init__(self, parent=None):
                super(NfcWindow, self).__init__(parent)
                Form, Window = uic.loadUiType("acceptWindow.ui")
                self.window = Window()
                self.form = Form()
                self.form.setupUi(self.window)

        def start(self):
            self.nextWindow=windows.acceptWindow()
            self.nextWindow.data=self.data
            self.nextWindow.sMaterial=self.sMaterial
            self.nextWindow.cMaterial=self.cMaterial



        def show(self):

                self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height());
                #self.window.show()
                self.window.showFullScreen()

                #then a animation:
                animation = QPropertyAnimation(self.window, b"pos");
                animation.setDuration(10000);
                animation.setStartValue(self.window.pos());
                animation.setEndValue(QPoint(0,0));

                #to slide in call
                animation.start();

