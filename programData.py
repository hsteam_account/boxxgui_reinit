from enum import Enum
import programs
class States(Enum):
    STARTING=1
    UV=2
    STEAM=3
    FIRSTDRY=4
    VAKUUM=5
    ODOR=6
    FINISH=7

class CMaterial:
        def __init__(self, name, uvValue, dryValue, ironValue):
            self.name=name
            self.uvValue=uvValue
            self.dryValue=dryValue
            self.ironValue=ironValue
            self.item=None

class SMaterial:
        def __init__(self, name, uvValue, dryValue):
            self.name=name
            self.uvValue=uvValue
            self.dryValue=dryValue
            self.item=None
class NfcData():
    def __init(self):
        pass
class Step():
    def __init__(self, state, time):
        self.state=state
        self.time=time
class Data():
    def __init__(self):
        self.shoeUVIndex=0
        self.shoeDryIndex=0
        self.clothUVIndex=0
        self.clothDryIndex=0
        self.clothIronIndex=0
        self.clothUVTime=10
        self.shoeUVTime=10
        self.shoeDryTime=10
        self.clothDryTime=10
        self.ironTime=0
        self.uvTime=10
        self.dryTime=10
        self.ventTime=1
        self.index=0
    def checkStepDone(self, progress):
        if self.currentTimeStep*60<progress:
            return True
        else:
            return False
    def getNextStep(self):
        if self.index>=len(self.steps):
            self.index-=1
        self.currentTimeStep=self.steps[self.index].time
        returnIndex=self.index
        self.index+=1
        return self.steps[returnIndex].state
    def calculateProgram(self):
        self.steps=[]
        self.uvTime=max(self.shoeUVTime, self.clothUVTime)
        self.dryTime=max(self.shoeDryTime, self.clothDryTime)
        #All
        if  self.uvTime!=0 and self.dryTime!=0  and self.ironTime!=0:
            self.steps+=self.uvState()
            self.steps.append(Step(programs.uVHeatState, 3))
            self.steps.append(Step(programs.steamState, self.ironTime))
            self.steps.append(Step(programs.airOutState, 1))
            remainingDryTime=self.dryTime
            while remainingDryTime>0:
                self.steps.append(Step(programs.circulationState, 2))
                self.steps.append(Step(programs.airOutState, .5))
                remainingDryTime-=2.5
        #UV only
        elif  self.uvTime!=0 and self.dryTime==0  and self.ironTime==0:
            self.steps+=self.uvState()
            self.steps.append(Step(programs.circulationState, self.uvTime/2))
        #Iron +UV
        elif  self.uvTime!=0 and self.dryTime==0  and self.ironTime!=0:
            self.steps+=self.uvState()
            self.steps.append(Step(programs.steamState, self.ironTime))
            self.steps.append(Step(programs.airOutState, 1))
            remainingDryTime=5
            while remainingDryTime>0:
                self.steps.append(Step(programs.circulationState, 2))
                self.steps.append(Step(programs.airOutState, .5))
                remainingDryTime-=2.5
        #Dry +UV
        elif  self.uvTime!=0 and self.dryTime!=0  and self.ironTime==0:
            self.steps+=self.uvState()
            self.steps.append(Step(programs.circulationState,3))
            self.steps.append(Step(programs.airOutState, 1))
            remainingDryTime=self.dryTime
            while remainingDryTime>0:
                self.steps.append(Step(programs.circulationState, 2))
                self.steps.append(Step(programs.airOutState, .5))
                remainingDryTime-=2.5
        #Iron +Dry
        elif  self.uvTime==0 and self.dryTime!=0  and self.ironTime!=0:
            self.steps.append(Step(programs.circulationState,3))
            self.steps.append(Step(programs.steamState,self.ironTime))
            self.steps.append(Step(programs.airOutState, 1))
            remainingDryTime=self.dryTime
            while remainingDryTime>0:
                self.steps.append(Step(programs.circulationState, 2))
                self.steps.append(Step(programs.airOutState, .5))
                remainingDryTime-=2.5
        #Iron only
        elif  self.uvTime==0 and self.dryTime==0  and self.ironTime!=0:
            self.steps.append(Step(programs.circulationState,3))
            self.steps.append(Step(programs.steamState,self.ironTime))
            self.steps.append(Step(programs.airOutState, 1))
            remainingDryTime=5
            while remainingDryTime>0:
                self.steps.append(Step(programs.circulationState, 2))
                self.steps.append(Step(programs.airOutState, .5))
                remainingDryTime-=2.5
        #Dry only
        elif  self.uvTime==0 and self.dryTime!=0  and self.ironTime==0:
            remainingDryTime=self.dryTime
            while remainingDryTime>0:
                self.steps.append(Step(programs.circulationState, 2))
                self.steps.append(Step(programs.airOutState, .5))
                remainingDryTime-=2.5
        else:
            print("ALL ZERO")
        self.steps.append(Step(programs.scentState, 2))
        self.steps.append(Step(programs.circulationState, 3))
        sum=0

        for step in self.steps:
            sum+=step.time
        self.fullTime=sum

    def uvState(self):
        steps=[]
        c=3
        #TODO add short and long uv
        longTime=max(self.shoeUVTime, self.clothUVTime)
        shortTime=min(self.shoeUVTime, self.clothUVTime)
        heatTimeStep=longTime-c
        if longTime==shortTime:
            steps.append(Step(programs.uVState, shortTime-c))
            steps.append(Step(programs.uVHeatState, c))
            return steps
        if heatTimeStep<shortTime:
            diff=shortTime-heatTimeStep
            steps.append(Step(programs.uVState, heatTimeStep))
            steps.append(Step(programs.uVHeatState, diff))
            if self.shoeUVTime<self.clothUVTime:
                steps.append(Step(programs.turnOffShoeUV, c-diff))
            else:
                steps.append(Step(programs.turnOffClothUV, c-diff))
        else:
            steps.append(Step(programs.uVState, shortTime))
            if self.shoeUVTime<self.clothUVTime:
                steps.append(Step(programs.turnOffShoeUV, heatTimeStep-shortTime))
            else:
                steps.append(Step(programs.turnOffClothUV, heatTimeStep-shortTime))
            steps.append(Step(programs.uVHeatState, c))
        return steps

