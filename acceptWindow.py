
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QTabWidget,QListWidget, QListView, QDial, QWidget,QHBoxLayout,QLCDNumber
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint,QThread

import os, sys
import datetime
import windows
from programData import CMaterial, SMaterial, Data, NfcData


class AcceptWindow(QObject):

        def __init__(self, parent=None):
                super(AcceptWindow, self).__init__(parent)
                Form, Window = uic.loadUiType("acceptWindow.ui")
                self.window = Window()
                self.form = Form()
                self.form.setupUi(self.window)
                self.startButton=self.getButton('startButton')
                self.startButton.clicked.connect(self.start)
                self.cancelButton=self.getButton('cancelButton')

                self.cancelButton.clicked.connect(self.cancel)

                self.shoeButtons=[self.getButton('sFast'),self.getButton('sRefreshed'),self.getButton('sThorough'),self.getButton('sDry'),self.getButton('sMoist'),self.getButton('sDripping')]
                self.clothButtons=[self.getButton('cFast'),self.getButton('cRefreshed'),self.getButton('cThorough'),self.getButton('cDry'),self.getButton('cMoist'),self.getButton('cDripping')]
                self.ironItem=self.getButton('cIron')
                self.shoeBanner=self.window.findChild(QWidget,'shoeBanner')
                self.clothBanner=self.window.findChild(QWidget,'clothBanner')
                self.sumTime=self.window.findChild(QLCDNumber,'sumTime')


        def cancel(self):
            self.home=windows.Home()
            self.window.close()
        def start(self):
            self.processWin = windows.ProcessWindow()
            #self.timer.stop()
            self.processWin.show()
            self.data.calculateProgram()
            self.processWin.start_Timer(self.data)
            self.processWin.nfc=self.nfc
            self.window.close()
        def init(self):
                if self.data.shoeNotSelected:
                    self.shoeBanner.hide()
                for i in range(len(self.shoeButtons)):
                    if self.data.shoeUVIndex==i or self.data.shoeDryIndex+3==i:
                        self.shoeButtons[i].show()
                    else:
                        self.shoeButtons[i].hide()
                if self.data.ironTime==0:
                    self.ironItem.hide()
                if self.data.clothNotSelected:
                    self.clothBanner.hide()
                for i in range(len(self.shoeButtons)):
                    if self.data.clothUVIndex==i or self.data.clothDryIndex+3==i:
                        self.clothButtons[i].show()
                    else:
                        self.clothButtons[i].hide()
                self.sumTime.display(self.data.fullTime)             
        def show(self):

                self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height());
                #self.window.show()
                self.window.showFullScreen()

                #then a animation:
                animation = QPropertyAnimation(self.window, b"pos");
                animation.setDuration(10000);
                animation.setStartValue(self.window.pos());
                animation.setEndValue(QPoint(0,0));

                #to slide in call
                animation.start();

        def getButton(self, name):
            return self.window.findChild(QPushButton,name)
