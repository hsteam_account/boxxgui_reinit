from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QTabWidget,QLCDNumber
from PyQt5.QtGui import QKeySequence, QPalette, QColor, QPixmap
from PyQt5.QtCore import Qt, QTimer, QObject, QFile, QPropertyAnimation,QPoint

import os, sys
from programData import CMaterial, SMaterial, Data
import datetime
import windows
def setClothingUVTime(button, owner):
    owner.data.clothUVTime=button.time
    owner.data.clothUVIndex=owner.odorTimes.index(button.time)
    owner.setTimes()
def setClothingDryTime(button, owner):
    owner.data.clothDryTime=button.time
    owner.data.clothDryIndex=owner.dryTimes.index(button.time)
    owner.setTimes()
def setClothingIronTime(button, owner):
    owner.data.ironTime=button.time
    owner.data.clothIronIndex=owner.ironTimes.index(button.time)
    owner.setTimes()
def setShoeUVTime(button, owner):
    owner.data.shoeUVTime=button.time
    owner.data.shoeUVIndex=owner.odorTimes.index(button.time)
    owner.setTimes()
def setShoeDryTime(button, owner):
    owner.data.shoeDryTime=button.time
    owner.data.shoeDryIndex=owner.dryTimes.index(button.time)
    owner.setTimes()
        
class Button():
    def __init__(self, normal, selected, time):
        self.normal=normal
        self.selected=selected
        self.time=time
    def select(self):
        self.func(self,self.owner)

    def selectButton(self, all):
        for item in all:
            item.selected.hide()
            item.normal.show()
        self.selected.show()
        self.normal.hide()
        self.select()

class ChooseWindow(QObject):

        def __init__(self, parent=None):
                super(ChooseWindow, self).__init__(parent)

                self.data=Data()
                ui_file = "start.ui"
                # self.line = self.window.findChild(QLineEdit, 'lineEdit')
                Form, Window = uic.loadUiType("chooseWindow.ui")
                self.window = Window()
                self.form = Form()
                self.form.setupUi(self.window)

                self.odorTimes=[0,10,30]
                self.dryTimes=[0,10,30]
                self.ironTimes=[0,5]


                #Clothing Odor Selection
                self.clothingOdorSelectionButtons=self.getSelectionButtons(['cFast', 'cRefreshed', 'cThorough'], self.odorTimes)
                self.initButtons(self.clothingOdorSelectionButtons,setClothingUVTime,0)
                self.data.clothUVTime=self.clothingOdorSelectionButtons[0].time
                #Clothing Dry Selection
                self.clothingDrySelectionButtons=self.getSelectionButtons(['cDry', 'cMoist', 'cDripping'], self.dryTimes)
                self.initButtons(self.clothingDrySelectionButtons,setClothingDryTime,0)
                self.data.clothDryTime=self.clothingDrySelectionButtons[0].time
                #Clothing Iron Selection
                self.clothingIronSelectionButtons=self.getSelectionButtons(['cIronNo', 'cIronYes', ], self.ironTimes)
                self.initButtons(self.clothingIronSelectionButtons,setClothingIronTime, 0)
                self.data.ironTime=self.clothingIronSelectionButtons[0].time
                #Shoes Odor Selection
                self.shoesOdorSelectionButtons=self.getSelectionButtons(['sFast', 'sRefreshed', 'sThorough'], self.odorTimes)
                self.initButtons(self.shoesOdorSelectionButtons,setShoeUVTime, 0)
                self.data.shoeUVTime=self.shoesOdorSelectionButtons[0].time
                #Shoes Dry Selection
                self.shoesDrySelectionButtons=self.getSelectionButtons(['sDry', 'sMoist', 'sDripping'], self.dryTimes)
                self.initButtons(self.shoesDrySelectionButtons,setShoeDryTime, 0)
                self.data.shoeDryTime=self.shoesDrySelectionButtons[0].time

                #Navigation
                backButton=self.getButton('backButton')
                backButton.clicked.connect(self.back)
                nextButton=self.getButton('nextButton')
                nextButton.clicked.connect(self.next)



                self.tabMain=self.window.findChild(QTabWidget,'tabMain')
                self.tabClothing=self.window.findChild(QTabWidget,'tabClothing')
                self.tabShoes=self.window.findChild(QTabWidget,'tabShoes')

                self.subTabs=[self.tabClothing,self.tabShoes]

                self.sumTime=self.window.findChild(QLCDNumber,'sumTime')
                self.setTimes()

        def back(self):
            mainIndex=self.tabMain.currentIndex()
            subIndex=self.subTabs[mainIndex].currentIndex()
            if mainIndex==0:
                if subIndex==0:
                    self.previousWindow=windows.MaterialWindow()
                    self.previousWindow.show()
                    self.window.hide()
                else:
                    self.subTabs[mainIndex].setCurrentIndex(subIndex-1)
            else:
                if subIndex==0:
                    self.tabMain.setCurrentIndex(mainIndex-1)
                    self.subTabs[mainIndex-1].setCurrentIndex(self.subTabs[mainIndex-1].count()-1)
                else:
                    self.subTabs[mainIndex].setCurrentIndex(subIndex-1)

        def next(self):
            mainIndex=self.tabMain.currentIndex()
            mainCount=self.tabMain.count()
            subIndex=self.subTabs[mainIndex].currentIndex()
            subCount=self.subTabs[mainIndex].count()

            if mainIndex==mainCount-1:
                if subIndex==subCount-1:
                    self.nextWindow=windows.ChoosePayment()
                    self.nextWindow.data=self.data
                    self.nextWindow.sMaterial=self.sMaterial
                    self.nextWindow.cMaterial=self.cMaterial
                    self.nextWindow.show()
                    self.window.close()
                else:
                    self.subTabs[mainIndex].setCurrentIndex(subIndex+1)
            else:
                if subIndex==subCount-1:
                    self.tabMain.setCurrentIndex(mainIndex+1)
                    self.subTabs[mainIndex+1].setCurrentIndex(0)
                else:
                    self.subTabs[mainIndex].setCurrentIndex(subIndex+1)

        def getSelectionButtons(self, ids, values):
            selectionButtons=[]
            for i in range(0,len(ids)):
                selectionButtons.append(Button(self.getButton(ids[i]+'Normal'), self.getButton(ids[i]+'Selected'),values[i] ))
            return selectionButtons
        def show(self):

                self.window.setGeometry(-self.window.width(), 0, self.window.width(), self.window.height());
                #self.window.show()
                self.window.showFullScreen()

                #then a animation:
                animation = QPropertyAnimation(self.window, b"pos");
                animation.setDuration(10000);
                animation.setStartValue(self.window.pos());
                animation.setEndValue(QPoint(0,0));

                #to slide in call
                animation.start();


        def initButtons(self, buttons, func, initialIndex):
                for button in buttons:
                    button.selected.hide()
                    button.owner=self
                    button.func=func
                    button.normal.clicked.connect(self.selectButtons(button, buttons))
                buttons[initialIndex].selected.show()
                buttons[initialIndex].normal.hide()
        def selectButtons(self, button, buttons):
                return lambda  x: button.selectButton( buttons)

        def getButton(self, name):
            return self.window.findChild(QPushButton,name)

        def init(self,sMaterial,cMaterial):
            self.sMaterial=sMaterial
            self.cMaterial=cMaterial
            if sMaterial.name=="None":
                self.subTabs=[self.tabClothing]
                self.tabMain.removeTab(1)
                self.data.shoeNotSelected=True
            else:
                self.data.shoeNotSelected=False

            if cMaterial.name=="None":
                self.subTabs=[self.tabShoes]
                self.tabMain.removeTab(0)
                self.data.clothNotSelected=True
            else:
                self.data.clothNotSelected=False
                if cMaterial.ironValue==0:
                    self.tabClothing.removeTab(2)
            self.tabMain.setCurrentIndex(0)
            self.tabClothing.setCurrentIndex(0)
            self.tabShoes.setCurrentIndex(0)
        def setTimes(self):
            self.data.calculateProgram()
            self.sumTime.display(self.data.fullTime)
            print(self.data.fullTime)